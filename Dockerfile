FROM python:3.9-slim-buster

LABEL maintainer="Jorge Brunal <diniremix@gmail.com>"

RUN mkdir /app

#Set flask_app as working directory
WORKDIR /app

#copy data from current dir into flask_app
COPY . /app

#install dependencies
RUN apt-get update
RUN apt-get install -y nano curl wget
RUN pip install --trusted-host pypi.python.org -r requirements.txt

#create container environment name
ENV VERSION 2022.08.22

# ENV FLASK_APP main.py
# ENV FLASK_ENV production
# ENV FLASK_DEBUG false
# ENV FLASK_RUN_PORT 8080

# Use this ports
EXPOSE 80
# for cloud run
EXPOSE 8080

# run main.py with gunicorn
# CMD ["gunicorn", "--bind", ":8080", "--workers", "10", "--threads", "5", "main:app"]
# 
# for flask on cloud run
# CMD ["gunicorn", "--bind", ":8080", "--workers", "10", "main:app"]
# 
# run main.py with python
CMD ["python","main.py"]
