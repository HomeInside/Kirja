# -*- coding: utf-8 -*-

from flet import UserControl, TextField
from utils import generate_uuid


class TextBox(UserControl):
    def __init__(
        self,
        label="caja de texto",
        hint_text=None,
        helper_text=None,
        max_length=None,
        password=False,
        on_change=None,
        on_submit=None,
        on_blur=None,
        id=None,
    ):
        super().__init__()
        self.label = label
        self.hint_text = hint_text
        self.helper_text = helper_text
        self.max_length = max_length
        self.password = password
        self.on_change = on_change
        self.on_submit = on_submit
        self.on_blur = on_blur

        uid = generate_uuid()
        str("textbox_{0}".format(uid)) if id is None else id

        self.id = str("textbox_{0}".format(uid)) if id is None else id

    # called after the UserControl added to a page and assigned transient id.
    def did_mount(self):
        # print("did_mount TextBox fn")
        pass

    # called before the UserControl is removed from a page.
    def will_unmount(self):
        # print("will_unmount fn")
        pass

    # method that is called to build control's UI and should returns a single Control instance
    # or a List of controls.
    def build(self):

        new_text = TextField(
            label=self.label,
            hint_text=self.hint_text,
            helper_text=self.helper_text,
            max_length=self.max_length,
            password=self.password,
            can_reveal_password=self.password,
            expand=True,
            on_change=self.on_change,
            on_submit=self.on_submit,
            on_blur=self.on_blur,
            data=id,
        )

        return new_text
