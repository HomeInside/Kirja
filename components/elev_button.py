# -*- coding: utf-8 -*-

from flet import UserControl, ElevatedButton


class ButtonElv(UserControl):
    def __init__(self, text="el boton El", disabled=False, on_click=None):
        super().__init__()
        self.text = text
        self.disabled = disabled
        self.on_click = None

    # called after the UserControl added to a page and assigned transient id.
    def did_mount(self):
        # print("did_mount TextBox fn")
        pass

    # called before the UserControl is removed from a page.
    def will_unmount(self):
        # print("will_unmount fn")
        pass

    # method that is called to build control's UI and should returns a single Control instance
    # or a List of controls.
    def build(self):

        elbtn = ElevatedButton(self.text, on_click=self.on_click)

        return elbtn
