# -*- coding: utf-8 -*-

from flet import UserControl, Dropdown, dropdown, Text


class ComboBox(UserControl):
    def __init__(
        self, label="el label del dropdown", option_list=["one", "two", "three"], label_position="left", on_change=None
    ):
        super().__init__()
        self.label = label
        self.option_list = option_list
        self.label_position = label_position
        self.on_change = on_change

    # called after the UserControl added to a page and assigned transient id.
    def did_mount(self):
        # print("did_mount fn")
        pass

    # called before the UserControl is removed from a page.
    def will_unmount(self):
        # print("will_unmount fn")
        pass

    # method that is called to build control's UI and should returns a single Control instance
    # or a List of controls.
    def build(self):

        t = Text(self.label, text_align="center", no_wrap=True)

        cbo_list = []
        for item in self.option_list:
            cbo_list.append(dropdown.Option(item))

        dropdown_comp = Dropdown(
            label=self.label,
            hint_text="Selecciona un elemento",
            options=cbo_list,
            on_change=self.on_change,
            autofocus=False,
        )

        return dropdown_comp
