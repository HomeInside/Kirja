# -*- coding: utf-8 -*-

from flet import UserControl, Checkbox


class CheckButton(UserControl):
    def __init__(self, label="el label del check", value=True, disabled=False, label_position="left"):
        super().__init__()
        self.label = label
        self.value = value
        self.disabled = disabled
        self.label_position = label_position

    # called after the UserControl added to a page and assigned transient id.
    def did_mount(self):
        # print("did_mount fn")
        pass

    # called before the UserControl is removed from a page.
    def will_unmount(self):
        # print("will_unmount fn")
        pass

    # method that is called to build control's UI and should returns a single Control instance
    # or a List of controls.
    def build(self):
        new_checkbox = Checkbox(
            label=self.label, value=self.value, disabled=self.disabled, label_position=self.label_position
        )
        return new_checkbox
