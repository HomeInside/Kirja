# -*- coding: utf-8 -*-

from flet import UserControl, Column, Container, Image, alignment, border_radius


class Picture(UserControl):
    def __init__(self, path=None, width=300, height=300, border_radius=10):
        super().__init__()
        self.path = None
        self.width = width
        self.height = height
        self.border_radius = border_radius

    # called after the UserControl added to a page and assigned transient id.
    def did_mount(self):
        # print("did_mount fn")
        # self.update()
        pass

    # called before the UserControl is removed from a page.
    def will_unmount(self):
        # print("will_unmount fn")
        pass

    # method that is called to build control's UI and should returns a single Control instance
    # or a List of controls.
    def build(self):
        base_uri = "https://picsum.photos/{0}/{1}?{2}".format(self.width, self.height, 1)

        img = Image(
            # src=f"https://picsum.photos/300/300?1",
            src=base_uri,
            width=self.width,
            height=self.height,
            fit="cover",
            repeat="noRepeat",
            border_radius=border_radius.all(10),
        )

        img_cont = Container(
            # return Container(
            content=img,
            alignment=alignment.center,
            # bgcolor=colors.AMBER,
            width=self.width,
            height=self.height,
        )

        # return img_cont

        return Column(controls=[img_cont])
