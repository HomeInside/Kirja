# -*- coding: utf-8 -*-

from flet import UserControl, Row, Image, border_radius


class Carousel(UserControl):
    def __init__(self, img_count=2, width=200, height=200, border_radius=10):
        super().__init__()
        self.img_count = img_count
        self.width = width
        self.height = height
        self.border_radius = border_radius
        # self.images = None

    # called after the UserControl added to a page and assigned transient id.
    def did_mount(self):
        # print("did_mount fn")
        # self.update()
        pass

    # called before the UserControl is removed from a page.
    def will_unmount(self):
        # print("will_unmount fn")
        pass

    # method that is called to build control's UI and should returns a single Control instance
    # or a List of controls.
    def build(self):
        images = Row(expand=1, wrap=False, scroll="always")

        base_uri = "https://picsum.photos/{0}/{1}".format(self.width, self.height)

        for i in range(0, self.img_count):
            images.controls.append(
                Image(
                    # src=f"https://picsum.photos/200/200?{i}",
                    src="{0}?{1}".format(base_uri, i),
                    width=self.width,
                    height=self.height,
                    fit="none",
                    repeat="noRepeat",
                    border_radius=border_radius.all(self.border_radius),
                )
            )

        return images
