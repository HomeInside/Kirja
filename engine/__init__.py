# -*- coding: utf-8 -*-

import os
import flet
from flet import Page, ListView, TextButton, Column, Row

from components import (
    Paragraph,
    Carousel,
    Picture,
    ComboBox,
    SliderBar,
    RadioButton,
    CheckButton,
    SwitchButton,
    TextBox,
    ButtonElv,
    ButtonText,
)
from flet import theme


def generate_list_comp():
    def dropdown_changed(e):
        print("dropdown_changed from main page")
        print("dropdown_changed value", e.control.value)

    def on_change(e):
        print("actualizando from main page")

    widget1 = TextBox("nombre", "escribe un nombre", max_length=15)
    widget2 = TextBox("contraseña", "maximo 32 caracteres", max_length=32, password=True)

    widget3 = SwitchButton("acepta TyC der", label_position="right")
    widget4 = SwitchButton("acepta TyC", value=False)
    widget5 = SliderBar("selecciona un valor", 1, 250, 20, on_change)
    widget6 = CheckButton("check listo aqui")
    widget7 = CheckButton("check listo aqui der", value=False, label_position="right")
    widget8 = RadioButton(
        "selecciona un color",
        option_list=[
            {"label": "Rojo", "value": "red"},
            {"label": "Green", "value": "green"},
            {"label": "Blue", "value": "blue"},
        ],
        label_position="right",
        on_change=on_change,
    )

    widget9 = ComboBox(
        "lista de continentes",
        option_list=["Africa", "America", "Asia", "Europa"],
        label_position="right",
        on_change=dropdown_changed,
    )

    widget10 = Picture()
    widget11 = Carousel(10)
    # widget11_1 = Carousel(10, 200, 200, 500)
    widget12 = Paragraph(
        "aqui va el titulo del parrafo",
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur quis nibh vitae purus consectetur facilisis sed vitae ipsum. Quisque faucibus sed nulla placerat sagittis. Phasellus condimentum risus vitae nulla vestibulum auctor. Curabitur scelerisque, nibh eget imperdiet consequat, odio ante tempus diam, sed volutpat nisl erat eget turpis. Sed viverra, diam sit amet blandit vulputate, mi tellus dapibus lorem, vitae vehicula diam mauris placerat diam. Morbi sit amet pretium turpis, et consequat ligula. Nulla velit sem, suscipit sit amet dictum non, tincidunt sed nulla. Aenean pellentesque odio porttitor sagittis aliquam. Nam varius at metus vitae vulputate. Praesent faucibus nibh lorem, eu pretium dolor dictum nec. Phasellus eget dui laoreet, viverra magna vitae, pellentesque diam.",
    )

    return [
        widget1,
        widget2,
        widget3,
        widget4,
        widget5,
        widget6,
        widget7,
        widget8,
        widget9,
        widget10,
        widget11,
        widget12,
    ]
