import os
import flet
from flet import Page, ListView, TextButton, Column, Row


from flet import theme
from engine import generate_list_comp
from components import SwitchButton, ButtonElv, ButtonText


def generate_comp(widget):
    comp = Column(
        controls=[
            Row(
                alignment="spaceBetween",
                vertical_alignment="center",
                controls=[widget],
            ),
            Column(
                spacing=25,
                controls=[
                    Column(),
                ],
            ),
        ]
    )
    return comp


def main2(page: Page):
    def main_theme(e):
        page.theme_mode = "dark" if page.theme_mode == "light" else "light"
        sw.label = "Light theme" if page.theme_mode == "light" else "Dark theme"
        page.update()

    def open_gridview(e):
        print("open_gridview fn activandose")
        page.update()

    def textbox_changed(e):
        print("e:", e.control)
        page.update()

    def on_change():
        print("actualizando from main page")
        page.update()

    def slider_changed(e):
        e.value = f"Slider changed to {e.control.value}"
        page.update()

    MAX_ITEMS = 10
    page.title = "main flet example"
    page.theme_mode = "light"
    # page.window_width = 400
    # page.window_height = 670
    # page.window_resizable = False
    page.horizontal_alignment = "center"
    # page.vertical_alignment = "center" # "spaceBetween"
    page.spacing = 10
    page.padding = 10
    page.update()

    sw = SwitchButton(label="Light theme", on_change=main_theme)

    elbtn = ButtonElv("using GridView", on_click=open_gridview)
    btn_txt = ButtonText("ButtonText con icono 1", icon="park_rounded", icon_color="green400", on_click=open_gridview)

    page.add(btn_txt)
    el = generate_comp(btn_txt)
    page.add(el)
    page.add(sw)
    page.add(elbtn)

    lv = ListView(expand=1, spacing=5, padding=10, auto_scroll=False)

    page.add(lv)

    comp_list = generate_list_comp()
    total_comp = len(comp_list)

    for i in range(0, total_comp, MAX_ITEMS):
        items = comp_list[i : (MAX_ITEMS + i)]
        lv.controls.extend(items)

        # Batch updates
        if i % MAX_ITEMS == 0:
            page.update()

    page.update()


if __name__ == "__main__":
    print("running python server from main")
    # desktop app
    flet.app(target=main2)
    #
    # web app
    # flet.app(target=main2, view=flet.WEB_BROWSER, port=8080)
    #
    # cloud run
    # flet.app(target=main2, host="0.0.0.0", port=8080)
    #
    # heroku
    # flet.app(target=main2, port=os.getenv("PORT"))
