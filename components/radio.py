# -*- coding: utf-8 -*-

from flet import UserControl, Column, RadioGroup, Radio, Text


class RadioButton(UserControl):
    def __init__(
        self,
        label="el label del radio",
        option_list=[{"label": "label 1", "value": "value1"}, {"label": "label 2", "value": "value2"}],
        disabled=False,
        label_position="left",
        on_change=None,
    ):
        super().__init__()
        self.label = label
        self.option_list = option_list
        self.disabled = disabled
        self.label_position = label_position
        self.on_change = on_change
        self.t = None

    # called after the UserControl added to a page and assigned transient id.
    def did_mount(self):
        # print("did_mount fn")
        pass

    # called before the UserControl is removed from a page.
    def will_unmount(self):
        # print("will_unmount fn")
        pass

    # method that is called to build control's UI and should returns a single Control instance
    # or a List of controls.
    def build(self):

        self.t = Text(self.label, text_align="center", no_wrap=True)

        col_items = []
        for item in self.option_list:
            col_items.append(
                Radio(value=item["value"], label=item["label"], label_position=self.label_position),
            )

        cg = RadioGroup(
            content=Column(col_items),
            on_change=self.on_change,
        )

        radio_comp = Column(
            [self.t, cg],
        )

        return radio_comp
