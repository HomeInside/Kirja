# -*- coding: utf-8 -*-

from flet import UserControl, Column, Slider, Text


class SliderBar(UserControl):
    def __init__(self, label="el label del slider", min=0, max=100, divisions=None, on_change=None):
        super().__init__()
        self.label = label
        self.min = min
        self.max = max
        self.divisions = divisions
        self.on_change = on_change

    # called after the UserControl added to a page and assigned transient id.
    def did_mount(self):
        # print("did_mount fn")
        pass

    # called before the UserControl is removed from a page.
    def will_unmount(self):
        # print("will_unmount fn")
        pass

    # method that is called to build control's UI and should returns a single Control instance
    # or a List of controls.
    def build(self):
        t = Text(self.label, text_align="center", no_wrap=True)

        new_slider = Slider(
            min=self.min, max=self.max, divisions=self.divisions, label="{value}%", on_change=self.on_change
        )

        slider_comp = Column(
            [t, new_slider],
        )

        return slider_comp
