# -*- coding: utf-8 -*-

from flet import Column, Text, UserControl


class Paragraph(UserControl):
    def __init__(self, title, content):
        super().__init__()
        self.title = title
        self.content = content

    # called after the UserControl added to a page and assigned transient id.
    def did_mount(self):
        # print("did_mount fn")
        pass

    # called before the UserControl is removed from a page.
    def will_unmount(self):
        # print("will_unmount fn")
        pass

    # method that is called to build control's UI and should returns a single Control instance
    # or a List of controls.
    def build(self):
        t = Text(self.title, style="headlineSmall")

        text_paragraph = Text(
            self.content,
            # width=800,
            # height=200,
            max_lines=25,
        )

        # return text_paragraph
        paragraph_comp = Column(
            [t, text_paragraph],
        )

        return paragraph_comp
