# Kirja

Componentes dinámicos usando [Flet](https://flet.dev)


# Requisitos

- [**Python**](https://www.python.org/downloads/) 3.9.x
- [**Poetry**](https://python-poetry.org) 1.1.x (Recomendado)
- [**Flet**](https://flet.dev) 0.1.52


## Instalación de este repositorio

Clonar este repositorio y alojarlo en una carpeta conveniente.

    git clone git@gitlab.com:HomeInside/Kirja.git

Se recomienda usar [**Poetry**](https://python-poetry.org) para desarrollo y pruebas.


## Activar Poetry en entornos Gnu/Linux, Mac OS

```sh
$ poetry env use python3.9
```


## Instalar las dependencias

Una vez dentro del entorno, instalar las dependencias:

```sh
$ poetry install
```


## Usando Poetry

```sh
$ poetry run python main.py
```


## Usando Poetry shell

```sh
$ poetry shell
(env) $ python main.py
```


## Despliegue en Heroku

```sh
heroku login
heroku git:remote -a kirja-app
git push heroku master
```


## Despliegue en Google Cloud Run

```sh
gcloud builds submit --config cloudbuild_run.yaml
```


## Licencia

El texto completo de la licencia puede ser encontrado en el archivo LGPL.txt


## Contacto
[Diniremix](https://gitlab.com/diniremix)

email: *diniremix [at] gmail [dot] com*
