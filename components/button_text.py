# -*- coding: utf-8 -*-

from flet import UserControl, TextButton, Column, Row


class ButtonText(UserControl):
    def __init__(self, text="el textboton", disabled=False, icon=None, icon_color=None, on_click=None):
        super().__init__()
        self.text = text
        self.disabled = disabled
        self.icon = disabled
        self.icon_color = disabled
        self.on_click = None

    # called after the UserControl added to a page and assigned transient id.
    def did_mount(self):
        # print("did_mount ButtonText fn")
        pass

    # called before the UserControl is removed from a page.
    def will_unmount(self):
        # print("will_unmount fn")
        pass

    # method that is called to build control's UI and should returns a single Control instance
    # or a List of controls.
    def build(self):

        elbtn = TextButton(
            self.text, disabled=self.disabled, icon=self.icon, icon_color=self.icon_color, on_click=self.on_click
        )

        # return Column([elbtn])
        # return Row([elbtn])
        return elbtn
